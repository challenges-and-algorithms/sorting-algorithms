from random import randint
from datetime import datetime as dt
from colorama import init, Fore
init(autoreset=True)


# # ============================================================
# # ============================================================
# # ============================================================


def random_number_generator(num):

    for i in range(num):

        yield randint(1, 1000000000)


# # ============================================================
# # ============================================================
# # ============================================================


def merge_sort(arr):

    if len(arr) <= 1:
        return

    mid = len(arr)//2
    left = arr[:mid]
    right = arr[mid:]

    merge_sort(left)
    merge_sort(right)

    merge_two_list(left, right, arr)


def merge_two_list(left, right, arr):
    len_left = len(left)
    len_right = len(right)

    i = j = k = 0

    while i < len_left and j < len_right:
        if left[i] <= right[j]:
            arr[k] = left[i]
            i += 1
        else:
            arr[k] = right[j]
            j += 1

        k += 1

    while i < len_left:
        arr[k] = left[i]
        i += 1
        k += 1

    while j < len_right:
        arr[k] = right[j]
        j += 1
        k += 1


# # ============================================================
# # ============================================================
# # ============================================================

number_of_index = int(input('number_of_index: '))


start_time = dt.now()
my_arr = [i for i in random_number_generator(number_of_index)]
print(
    f'Random list generated in: {Fore.GREEN}{dt.now() - start_time}\n')



start_time = dt.now()
merge_sort(my_arr)
print(
    f'Random list Bubble-Sorted in: {Fore.GREEN}{dt.now() - start_time}\n')

print(my_arr)