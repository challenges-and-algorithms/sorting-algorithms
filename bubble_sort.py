from random import randint
from datetime import datetime as dt
from colorama import init, Fore
init(autoreset=True)


# # ============================================================
# # ============================================================
# # ============================================================


def random_number_generator(num):

    for i in range(num):

        yield randint(1, 1000000000)


# # ============================================================
# # ============================================================
# # ============================================================


def bubble_sort(arr):

    size = len(arr)

    for i in range(size):

        for j in range(i, size):

            if arr[i] > arr[j]:

                arr[i], arr[j] = arr[j], arr[i]



# # ============================================================
# # ============================================================
# # ============================================================

number_of_index = int(input('number_of_index: '))


start_time = dt.now()
my_arr = [i for i in random_number_generator(number_of_index)]
print(
    f'Random list generated in: {Fore.GREEN}{dt.now() - start_time}\n')



start_time = dt.now()
bubble_sort(my_arr)
print(
    f'Random list Bubble-Sorted in: {Fore.GREEN}{dt.now() - start_time}\n')

print(my_arr)